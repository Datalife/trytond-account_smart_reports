datalife_account_smart_reports
==============================

The account_smart_reports module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_smart_reports/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_smart_reports)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
